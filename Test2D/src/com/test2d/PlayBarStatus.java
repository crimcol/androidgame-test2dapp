package com.test2d;

import java.util.Collections;
import java.util.Stack;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.FadeInModifier;
import org.andengine.entity.modifier.FadeOutModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;

public class PlayBarStatus extends HUD implements IPlayBarStatus {

	private final int DEFAULT_LIVES_COUNT = 3;
	
	private ResourcesManager resources;
	private float barWidth;
	private float barHeight;
	private Text scores;
	private int score;
	private Stack<Sprite> livesList;
	private SequenceEntityModifier liveDeletingModifier;
	private Stack<IEntity> listToDetach;
	
	public PlayBarStatus(ResourcesManager pResources)
	{
		resources = pResources;
		barWidth = resources.camera.getWidth();
		barHeight = 50;	
		score = 0;
		livesList = new Stack<Sprite>();
		listToDetach = new Stack<IEntity>();
		
		createStatusBar();
	}
	
	private void createStatusBar()
	{
		Rectangle gameStatus = new Rectangle(barWidth / 2, barHeight / 2 + 5, barWidth, barHeight, resources.vbom);
		gameStatus.setColor(0.1f, 0.1f, 0.1f, 0.43f);
		
		scores = new Text(0, 0, resources.ScoreFont, "00000", 6, resources.vbom);
		scores.setOffsetCenter(0, 0.5f);
		scores.setX(barWidth - scores.getWidth() - 10);
		scores.setY(gameStatus.getY() - 17);
		gameStatus.attachChild(scores);
		
		Text scoreText = new Text (0, 0, resources.ScoreFont, "SCORE:", resources.vbom);
		scoreText.setOffsetCenter(1, 0.5f);
		scoreText.setX(scores.getX() - 8);
		scoreText.setY(scores.getY());
		gameStatus.attachChild(scoreText);
		
		createLives(gameStatus, DEFAULT_LIVES_COUNT);
		
		super.attachChild(gameStatus);
	}
	
	private void createLives(IEntity root, int pLives)
	{
		float regionWidth = resources.LivePictureRegion.getWidth();
		float offset = regionWidth - 40;
		
		for (int i = pLives - 1; i >= 0; i--)
		{
			Sprite spriteLive = new Sprite(
					(i * offset) + regionWidth / 2, 20, 
					resources.LivePictureRegion, 
					resources.vbom);
			root.attachChild(spriteLive);
			livesList.push(spriteLive);
		}
		Collections.reverse(livesList);
		
		liveDeletingModifier = new SequenceEntityModifier(
				new FadeOutModifier(0.1f),
				new FadeInModifier(0.1f),
				new FadeOutModifier(0.1f),
				new FadeInModifier(0.1f),
				new FadeOutModifier(0.1f),
				new FadeInModifier(0.1f),
				new FadeOutModifier(0.3f),
				new FadeInModifier(0.3f),
				new FadeOutModifier(0.3f)
				{
					@Override
					protected void onModifierFinished(IEntity pItem) {
						listToDetach.push(pItem);
						super.onModifierFinished(pItem);
					}
				});
		liveDeletingModifier.setAutoUnregisterWhenFinished(true);
	}
	
	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		while (listToDetach.size() > 0)
		{
			listToDetach.pop().detachSelf();
		}
		super.onManagedUpdate(pSecondsElapsed);
	}
	
	// IPlayBarStatus
	@Override
	public void reduceLive() {
		int liveCount = livesList.size();
		if (liveCount == 0)
			return;
		
		Sprite spriteLive = livesList.pop();
		spriteLive.registerEntityModifier(liveDeletingModifier);
		liveDeletingModifier.reset();
	}

	@Override
	public void addScore(int pScore) 
	{
		score += pScore;
		scores.setText(String.valueOf(score));
	}

	@Override
	public void resetStatus() {
		// TODO Auto-generated method stub
		
	}
}
