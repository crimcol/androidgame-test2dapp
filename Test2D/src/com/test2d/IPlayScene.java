package com.test2d;

import org.andengine.entity.IEntity;

public interface IPlayScene {

	IEntity getStonesLayer();
	IEntity getPlayerLayer();
	IEntity getPoliceLayer();
	void setDebugText(String text);
	void setLifes(int count);
	void addScore(int pScore);
}
