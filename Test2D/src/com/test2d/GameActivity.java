package com.test2d;

import java.io.IOException;

import org.andengine.engine.Engine;
import org.andengine.engine.LimitedFPSEngine;
import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.BaseGameActivity;

import com.test2d.controllers.ControllerFactory;
import com.test2d.controllers.ISceneController;
import com.test2d.entity.RandomPoint;

import android.view.KeyEvent;

public class GameActivity extends BaseGameActivity {
	
	private static final int CAMERA_WIDTH = 800;
	private static final int CAMERA_HEIGHT = 480;
	
	private BoundCamera m_camera;
	private ControllerFactory m_controllerFactory;
	ISceneController m_controller;

	@Override
	public EngineOptions onCreateEngineOptions() {
		
		m_camera = new BoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		RandomPoint.setCamera(m_camera);
		
		final EngineOptions engineOptions = new EngineOptions(
				true, 
				ScreenOrientation.LANDSCAPE_SENSOR, 
				new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), 
				m_camera);
		engineOptions.getTouchOptions().setNeedsMultiTouch(false);
		engineOptions.getAudioOptions().setNeedsSound(true);
		
		return engineOptions;
	}
	
	@Override
	public Engine onCreateEngine(final EngineOptions pEngineOptions) {
		return new LimitedFPSEngine(pEngineOptions, 60);
	}

	@Override
	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws IOException {
 
		ResourcesManager.prepareManager(mEngine, this, m_camera, getVertexBufferObjectManager());
		m_controllerFactory = new ControllerFactory();
	    pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws IOException 
	{
		m_controller = m_controllerFactory.getStartController();
		m_controller.showScene();
		pOnCreateSceneCallback.onCreateSceneFinished(m_controller.getScene());
	}

	@Override
	public void onPopulateScene(Scene pScene,
			OnPopulateSceneCallback pOnPopulateSceneCallback)
			throws IOException {

		mEngine.registerUpdateHandler(new TimerHandler(2f, new ITimerCallback() 
	    {
	            public void onTimePassed(final TimerHandler pTimerHandler) 
	            {
	                mEngine.unregisterUpdateHandler(pTimerHandler);
	                // load menu resources, create menu scene
	                // set menu scene using scene manager
	                // disposeSplashScene();
	                // READ NEXT ARTICLE FOR THIS PART.
	            }
	    }));
	    pOnPopulateSceneCallback.onPopulateSceneFinished();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.exit(0);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{  
	    if (keyCode == KeyEvent.KEYCODE_BACK)
	    {
	    	SceneManager.getInstance().getCurrentScene().onBackKeyPressed();
	    }
	    return false; 
	}
}
