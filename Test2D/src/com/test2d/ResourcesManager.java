package com.test2d;

import java.io.IOException;

import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.extension.svg.opengl.texture.atlas.bitmap.SVGBitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.graphics.Color;

public class ResourcesManager {

	//---------------------------------------------
    // VARIABLES
    //---------------------------------------------
    
    private static final ResourcesManager INSTANCE = new ResourcesManager();
    
    private BitmapTextureAtlas m_bitmapTextureAtlas;
    public TiledTextureRegion BrontTextureRegion;
    
    public Engine engine;
    public GameActivity activity;
    public Camera camera;
    public VertexBufferObjectManager vbom;
    
    //---------------------------------------------
    // TEXTURES & TEXTURE REGIONS
    //---------------------------------------------
    
    public Font MainFont;
    public Font ScoreFont;
    public Font ButtonFont;
    
    private ITexture m_BackgroundTexture;
    private ITexture m_GameTexture;
    private ITexture m_GameOverTexture;
    public ITextureRegion BackgroundTextureRegion;
    public ITiledTextureRegion PlayerTextureRegion;
    public ITextureRegion Stone1TextureRegion;
    public ITextureRegion Stone2TextureRegion;
    public ITextureRegion PoliceTextureRegion;
    public ITiledTextureRegion SplashBomTextureRegion;
    public ITextureRegion LivePictureRegion;
    public Sound JumpSound;
    public Sound PoliceBamSound;
    public Sound PlayerCaughtSound;
    
    public ITextureRegion NewGameButton;
    
    //---------------------------------------------
    // CLASS LOGIC
    //---------------------------------------------
    
    public void loadStartScene()
    {
    	loadGameFonts();
    }
    
    public void unloadStartScene()
    {
    	unloadGameFonts();
    }
    
    public void loadGamePlayScene()
    {
    	loadGameFonts();
    	loadGraphics();
    	loadSounds();
    }
    
    public void unloadGamePlayScene()
    {
    	unloadGameFonts();
    	unloadGraphics();
    	unloadSounds();
    }
    
    public void loadGameOverScene()
    {
    	loadGameOverGraphics();
    	loadGameFonts();
    }
    
    public void unloadGameOverScene()
    {
    	unloadGameOverGraphics();
    	unloadGameFonts();
    }

    public void loadTestScreen()
    {
    	BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		
		m_bitmapTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 64, 160, TextureOptions.BILINEAR);
		BrontTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(m_bitmapTextureAtlas, activity, "bront1_tiled.png", 0, 0, 1, 5);
		m_bitmapTextureAtlas.load();
    }
    
    public void unloadTestScreen()
    {
    	m_bitmapTextureAtlas.unload();
    	BrontTextureRegion = null;
    }
    
    private void loadGameFonts()
    {
    	FontFactory.setAssetBasePath("font/");
        final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        MainFont = FontFactory.createStrokeFromAsset(
        		activity.getFontManager(), 
        		mainFontTexture, 
        		activity.getAssets(), 
        		"jokerman_let.ttf", 
        		50f, 
        		true, 
        		Color.BLACK, 
        		2, 
        		Color.WHITE);
        MainFont.load();
        
        final ITexture scoreFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        ScoreFont = FontFactory.createFromAsset(
        		engine.getFontManager(), 
        		scoreFontTexture, 
        		activity.getAssets(), 
        		"jokerman_let.ttf", 45, true, Color.WHITE);
        
        final ITexture buttonFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        ButtonFont = FontFactory.createFromAsset(
        		engine.getFontManager(), 
        		buttonFontTexture, 
        		activity.getAssets(), 
        		"jokerman_let.ttf", 25, true, Color.WHITE);
        		
        MainFont.load();
        ScoreFont.load();
        ButtonFont.load();
    }
    
    private void unloadGameFonts()
    {
    	MainFont.unload();
    	MainFont = null;
    	ScoreFont.unload();
    	ScoreFont = null;
        ButtonFont.unload();
        ButtonFont = null;
    }
    
    private void loadGraphics()
    {
    	try {
			if (m_BackgroundTexture == null)
			{
				//m_BackgroundTexture = new BitmapTextureAtlas(engine.getTextureManager(), 64, 64);
				//BackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset((BitmapTextureAtlas)m_BackgroundTexture, activity.getAssets(), "gfx/game_background.png", 0, 0);
				m_BackgroundTexture = new AssetBitmapTexture(engine.getTextureManager(), activity.getAssets(), "gfx/game_background.png", TextureOptions.REPEATING_NEAREST);
				BackgroundTextureRegion = TextureRegionFactory.extractFromTexture(m_BackgroundTexture);
			}
			if (m_GameTexture == null)
			{
				BuildableBitmapTextureAtlas buildableBitmapTextureAtlas = new BuildableBitmapTextureAtlas(engine.getTextureManager(), 512, 512);
				PlayerTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(buildableBitmapTextureAtlas, activity, "gfx/player_seq.png", 3, 3);
				//BackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(buildableBitmapTextureAtlas, activity, "gfx/game_background_64.png");
				Stone1TextureRegion = SVGBitmapTextureAtlasTextureRegionFactory.createFromAsset(buildableBitmapTextureAtlas, activity, "gfx/stone1.svg", 21, 29);
				Stone2TextureRegion = SVGBitmapTextureAtlasTextureRegionFactory.createFromAsset(buildableBitmapTextureAtlas, activity, "gfx/stone2.svg", 20, 25);
				PoliceTextureRegion = SVGBitmapTextureAtlasTextureRegionFactory.createFromAsset(buildableBitmapTextureAtlas, activity, "gfx/police_car.svg", 66, 34);
				SplashBomTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(buildableBitmapTextureAtlas, activity, "gfx/splash_seq.png", 1, 5);
				LivePictureRegion = SVGBitmapTextureAtlasTextureRegionFactory.createFromAsset(buildableBitmapTextureAtlas, activity, "gfx/live_picture.svg", 112, 85);
				
				buildableBitmapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
				m_GameTexture = buildableBitmapTextureAtlas;
			}
		}
    	catch (final TextureAtlasBuilderException e) {
    		e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		m_GameTexture.load();
		m_BackgroundTexture.load();
    }
    
    public void unloadGraphics()
    {
    	m_GameTexture.unload();
    	m_BackgroundTexture.unload();
    }
    
    public void loadSounds()
    {
    	try {
			if (JumpSound == null)
				JumpSound = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "mfx/jump.mp3");
			if (PoliceBamSound == null)
				PoliceBamSound = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "mfx/splash_bam.mp3");
			if (PlayerCaughtSound == null)
				PlayerCaughtSound = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "mfx/player_caught.mp3");
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public void unloadSounds()
    {
    	JumpSound.release();
    	JumpSound = null;
    	PoliceBamSound.release();
    	PoliceBamSound = null;
    	PlayerCaughtSound.release();
    	PlayerCaughtSound = null;
    }
    
    private void loadGameOverGraphics()
    {
    	try {
	    	if (m_GameOverTexture == null)
    		{
	    		BuildableBitmapTextureAtlas buildableBitmapTextureAtlas = new BuildableBitmapTextureAtlas(engine.getTextureManager(), 256, 64);
		    	NewGameButton = SVGBitmapTextureAtlasTextureRegionFactory.createFromAsset(buildableBitmapTextureAtlas, activity, "gfx/new_game_button.svg", 148, 45);
				buildableBitmapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
				
				m_GameOverTexture = buildableBitmapTextureAtlas;
    		}
		} catch (TextureAtlasBuilderException e) {
			e.printStackTrace();
		}
    	
    	m_GameOverTexture.load();
    }
    
    private void unloadGameOverGraphics()
    {
    	m_GameOverTexture.unload();
    	m_GameOverTexture = null;
    }
    
    /**
     * @param engine
     * @param activity
     * @param camera
     * @param vbom
     * <br><br>
     * We use this method at beginning of game loading, to prepare Resources Manager properly,
     * setting all needed parameters, so we can latter access them from different classes (eg. scenes)
     */
    public static void prepareManager(Engine engine, GameActivity activity, Camera camera, VertexBufferObjectManager vbom)
    {
        getInstance().engine = engine;
        getInstance().activity = activity;
        getInstance().camera = camera;
        getInstance().vbom = vbom;
    }
    
    //---------------------------------------------
    // GETTERS AND SETTERS
    //---------------------------------------------
    
    public static ResourcesManager getInstance()
    {
        return INSTANCE;
    }
}
