package com.test2d.controllers;

import com.test2d.ResourcesManager;
import com.test2d.SceneManager;

import Scenes.BaseScene;

public class StartController extends BaseController
{
	public StartController(SceneManager pSceneManager, ResourcesManager pResoursesManager, ControllerFactory pControllerFactory)
	{
		super(pSceneManager, pResoursesManager, pControllerFactory);
	}

	@Override
	public void showScene()
	{
		sceneManager().createStartScene(this);
	}

	@Override
	public void destroy()
	{
		sceneManager().disposeStartScene();
	}

	@Override
	public BaseScene getScene()
	{
		return sceneManager().getCurrentScene();
	}
	
	public void closeGame()
	{
		destroy();
		resources().activity.finish();
	}
	
	public void goToGamePlay()
	{
		destroy();
		controllerFactory().getGamePlayController().showScene();
	}
}
