package com.test2d.controllers;

import com.test2d.ResourcesManager;
import com.test2d.SceneManager;

public abstract class BaseController implements ISceneController
{
	private SceneManager m_SceneManager;
	private ResourcesManager m_ResoursesManager;
	private ControllerFactory m_Controllerfactory;
	
	public BaseController(SceneManager pSceneManager, ResourcesManager pResoursesManager, ControllerFactory pControllerFactory)
	{
		setSceneManager(pSceneManager);
		setResoursesManager(pResoursesManager);
		setControllerFactory(pControllerFactory);
	}
	
	protected SceneManager sceneManager()
	{
		return m_SceneManager;
	}
	
	private void setSceneManager(SceneManager pSceneManager)
	{
		m_SceneManager = pSceneManager;
	}
	
	protected ResourcesManager resources()
	{
		return m_ResoursesManager;
	}
	
	private void setResoursesManager(ResourcesManager pResoursesManager)
	{
		m_ResoursesManager = pResoursesManager;
	}
	
	protected ControllerFactory controllerFactory()
	{
		return m_Controllerfactory;
	}
	
	private void setControllerFactory(ControllerFactory pControllerFactory)
	{
		m_Controllerfactory = pControllerFactory;
	}
}
