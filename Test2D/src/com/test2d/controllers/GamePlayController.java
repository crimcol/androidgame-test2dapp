package com.test2d.controllers;

import java.util.*;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.input.touch.TouchEvent;
import org.andengine.util.adt.color.Color;

import Scenes.BaseScene;
import Scenes.GamePlayScene;

import com.test2d.IPlayScene;
import com.test2d.ResourcesManager;
import com.test2d.SceneManager;
import com.test2d.engine.*;
import com.test2d.entity.*;
import com.test2d.entity.Player.JumpProgress;

public class GamePlayController extends BaseController implements IUpdateHandler, IOnSceneTouchListener
{
	private TimerHandlerExtended gameResetHandler;
	private TimerHandlerExtended gameScoring;
	private IPlayScene playScene;
	private List<Stone> stones;
	private List<Police> cars;
	private LifesManager playerLifes;
	
	public Player player;
	
	// For debug
	private Line debugNewDirectionLine;
	
	public GamePlayController(SceneManager pSceneManager, ResourcesManager pResoursesManager, ControllerFactory pControllerFactory)
	{
		super(pSceneManager, pResoursesManager, pControllerFactory);
	}
	
	//ISceneController
	@Override
	public void showScene() 
	{
		sceneManager().createGamePlayScene(this);
		GamePlayScene scene = (GamePlayScene)sceneManager().getCurrentScene();
		playScene = (IPlayScene)scene;
		
		player = scene.createPlayer();
		stones = scene.createStones();
		cars = scene.createPolice();
		
		debugNewDirectionLine = createDebugNewDirectionLine();
		
		gameResetHandler = new TimerHandlerExtended(2, new ITimerCallback() 
	    {
            public void onTimePassed(final TimerHandler pTimerHandler) 
            {
            	resetGame();
            }
	    }, false);
		resources().engine.registerUpdateHandler(gameResetHandler);
		
		gameScoring = new TimerHandlerExtended(0.1f, new ITimerCallback()
		{
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
				playScene.addScore(1);
			}
		});
		gameScoring.setAutoReset(true);
		resources().engine.registerUpdateHandler(gameScoring);
		
		playerLifes = new LifesManager();
		playerLifes.setListener(new ILifesChangedListener() 
		{
			@Override
			public void lifeCountChanged(int currentCount) {
				playScene.setLifes(currentCount);
			}
		});
		
		scene.LazyInit();
	}

	@Override
	public BaseScene getScene() {
		return (BaseScene)playScene;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	
	// IUpdateHandler
	@Override
	public void reset() { }
	
	@Override
	public void onUpdate(float pSecondsElapsed) 
	{
		RandomPoint.updateBoxPosition();
		
		stonesCollidesWithPlayer();
		policeCollidesWithPolice();
		policeCollidesWithPlayer();
	}
	
	// IOnSceneTouchListener
	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		float sX = player.getSprite().getX();
		float sY = player.getSprite().getY();
		float tX = pSceneTouchEvent.getX();
		float tY = pSceneTouchEvent.getY();
		debugNewDirectionLine.setPosition(sX, sY, tX, tY);
		
		float targetAngle = DegreeConverter.PolarByPoint(tX - sX, sY - tY);
		player.setTargetAngle(targetAngle);
		
		return true;
	}
	
	// Public methods
	
	public void goToStartScene()
	{
		sceneManager().disposeGamePlayScene();
		controllerFactory().getStartController().showScene();
	}
	
	public void goToGameOverScene()
	{
		sceneManager().disposeGamePlayScene();
		controllerFactory().getGameOverController().showScene();
	}
	
	// Private methods
	
	private void stonesCollidesWithPlayer()
	{
		for (Stone s : stones)
		{
			if (s.collidesWith(player.getSprite().getBody()))
			{
				if (player.getJumpProgress() == JumpProgress.GoDown
						|| player.getJumpProgress() == JumpProgress.NoJump)
				{
					player.Jump();
					playScene.addScore(10);
				}
			}
		}
	}
	
	private void policeCollidesWithPolice()
	{
		for (int i =0; i<cars.size()-1; i++)
		{
			Police p1 = cars.get(i);
			if (!p1.isDriving())
				continue;
			for (int j=i+1; j<cars.size(); j++)
			{	
				Police p2 = cars.get(j);
				if (p2.isDriving() && p1.getBody().collidesWith(p2.getBody()))
				{
					p1.bang();
					p2.bang();
					resources().PoliceBamSound.play();
					p1.startChase();
					p2.startChase();
					playScene.addScore(50 * 2);
				}
			}
		}
	}
	
	private void policeCollidesWithPlayer()
	{
		for (int i =0; i<cars.size(); i++)
		{
			Police p1 = cars.get(i);
			if (!p1.isDriving())
				continue;
			if (p1.getBody().collidesWith(player.getBody()))
			{
				if (player.getJumpProgress() == JumpProgress.GoDown)
				{
					player.Jump();
					playScene.addScore(100);
				}
				else if (!player.IsJumping())
				{
					gameScoring.Disable();
					player.Die();
					for (Police car : cars)
						car.Stop();
					
					gameResetHandler.reset();
					playerLifes.decreaseLife();
					break;
				}
			}
		}
	}
	
	private void resetGame()
	{
		gameScoring.Enable();
		player.Start();
		
		for (Police car : cars)
		{
			car.startChase();
		}
	}
	
	private Line createDebugNewDirectionLine()
	{
		Line debugLine = new Line(0, 0, 0, 0, 5, resources().vbom);
		debugLine.setColor(Color.GREEN);
		playScene.getPlayerLayer().attachChild(debugLine);
		
		return debugLine;
	}
	
	private class LifesManager
	{
		private final int DEFAULT_COUNT = 3;
		private int currentCount;
		private ILifesChangedListener listener;
		
		public LifesManager()
		{
			currentCount = DEFAULT_COUNT;
			setListener(new ILifesChangedListener() 
			{
				@Override
				public void lifeCountChanged(int currentCount) {	
				}
			});
		}
		
		public int getLifesCount()
		{
			return currentCount;
		}
		
		public void decreaseLife()
		{
			if (currentCount > -1)
			{
				currentCount -= 1;
				listener.lifeCountChanged(getLifesCount());
			}
		}
		
		public void setListener(ILifesChangedListener pListener)
		{
			if (pListener != null)
				listener = pListener;
		}
	}
	
	private interface ILifesChangedListener
	{
		void lifeCountChanged(int currentCount);
	}
}
