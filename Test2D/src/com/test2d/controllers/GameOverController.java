package com.test2d.controllers;

import Scenes.BaseScene;

import com.test2d.ResourcesManager;
import com.test2d.SceneManager;

public class GameOverController extends BaseController
{
	public GameOverController(SceneManager pSceneManager, ResourcesManager pResoursesManager, ControllerFactory pControllerFactory) 
	{
		super(pSceneManager, pResoursesManager, pControllerFactory);
	}

	@Override
	public void showScene() {
		sceneManager().createGameOverScene(this);
	}

	@Override
	public BaseScene getScene() 
	{
		return sceneManager().getCurrentScene();
	}

	@Override
	public void destroy() {
		sceneManager().disposeGameOverScene();
	}

	public void goToGamePlay()
	{
		destroy();
		controllerFactory().getGamePlayController().showScene();
	}
	
	public void goToStartScene()
	{
		destroy();
		controllerFactory().getStartController().showScene();
	}
}
