package com.test2d.controllers;

import Scenes.BaseScene;

public interface ISceneController
{
	void showScene();
	BaseScene getScene();
	void destroy();
}
