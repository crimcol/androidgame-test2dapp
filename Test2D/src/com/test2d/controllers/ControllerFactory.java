package com.test2d.controllers;

import com.test2d.ResourcesManager;
import com.test2d.SceneManager;

public class ControllerFactory
{
	private SceneManager m_SceneManager;
	private ResourcesManager m_ResourcesManager;
	
	public ControllerFactory()
	{
		m_ResourcesManager = ResourcesManager.getInstance();
		m_SceneManager = SceneManager.getInstance();
	}
	
	public ISceneController getStartController()
	{
		return new StartController(m_SceneManager, m_ResourcesManager, this);
	}
	
	public ISceneController getGameOverController()
	{
		return new GameOverController(m_SceneManager, m_ResourcesManager, this);
	}
	
	public ISceneController getGamePlayController()
	{
		return new GamePlayController(m_SceneManager, m_ResourcesManager, this);
	}
}
