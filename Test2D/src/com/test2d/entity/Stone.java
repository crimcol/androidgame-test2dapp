package com.test2d.entity;

import org.andengine.entity.sprite.Sprite;

import com.test2d.ResourcesManager;

import android.graphics.PointF;

public class Stone extends Sprite
{
	public Stone(float pX, float pY, ResourcesManager pResources) 
	{
		super(pX, pY, 
			Math.random() > 0.5 ? pResources.Stone1TextureRegion : pResources.Stone2TextureRegion, 
			pResources.vbom);
	}
	
	@Override
	protected void onManagedUpdate(float pSecondsElapsed) 
	{
		updatePosition();
		super.onManagedUpdate(pSecondsElapsed);
	}
	
	private void updatePosition()
	{
		if (RandomPoint.isInsideBox(getX(), getY()))
			return;
		
		PointF newPos = RandomPoint.getOutsidePoint();
		this.setPosition(newPos.x, newPos.y);
		updateView();
	}
	
	private void updateView()
	{
		this.setScale(0.5f + (float)Math.random());
		this.setRotation((float)Math.random() * 360);
	}
}