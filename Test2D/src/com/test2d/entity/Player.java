package com.test2d.entity;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

import com.test2d.ResourcesManager;
import com.test2d.engine.DegreeConverter;
import com.test2d.engine.PlayerPhysicsHandler;

public class Player
{
	private static float SCALE_UP_SPEED = 0.5f / 0.3f;
	
	private ResourcesManager resources;
	private PlayerAnimatedSprite playerSprite;
	private SequenceEntityModifier jumpModifier;
	private ScaleModifier jumpStartModifier;
	private JumpProgress jumpState;
	
	public PlayerPhysicsHandler mPhysicsHandler;
	
	public Player(ResourcesManager pResources)
	{
		resources = pResources;
		jumpState = JumpProgress.NoJump;
	}
	
	public PlayerAnimatedSprite getSprite()
	{
		return playerSprite;
	}
	
	public IEntity getBody()
	{
		return getSprite().getBody();
	}
	
	public void load()
	{		
		loadMainSprite();
		
		final IEntity sprite = getSprite();
		this.mPhysicsHandler = new PlayerPhysicsHandler(sprite);
		sprite.registerUpdateHandler(this.mPhysicsHandler);
		//this.mPhysicsHandler.setSpeed(0f);
		
		jumpStartModifier = new ScaleModifier(0.3f, 1, 1.5f)
		{
			@Override
			protected void onModifierStarted(IEntity pItem) {
				jumpState = JumpProgress.GoUp;
				super.onModifierStarted(pItem);
			}
		}; 
		jumpModifier = new SequenceEntityModifier(
				jumpStartModifier,
				new ScaleModifier(0.3f, 1.5f, 1.5f)
				{
					@Override
					protected void onModifierStarted(IEntity pItem) {
						jumpState = JumpProgress.OnTop;
						super.onModifierStarted(pItem);
					}
				},
				new ScaleModifier(0.2f, 1.5f, 1.25f),
				new ScaleModifier(0.2f, 1.25f, 1)
				{
					@Override
					protected void onModifierStarted(IEntity pItem) {
						jumpState = JumpProgress.GoDown;
						super.onModifierStarted(pItem);
					}
					@Override
					protected void onModifierFinished(IEntity pItem) {
						jumpState = JumpProgress.NoJump;
						super.onModifierFinished(pItem);
					}
				});
		
		jumpModifier.setAutoUnregisterWhenFinished(false);
		sprite.registerEntityModifier(jumpModifier);
	}
	
	public JumpProgress getJumpProgress()
	{
		return jumpState;
	}
	
	public boolean IsJumping()
	{
		return jumpState != JumpProgress.NoJump; 
	}
	
	public void Jump()
	{
		float scaleUpSpeed = (1.5f - getSprite().getScaleX()) / SCALE_UP_SPEED;
		jumpStartModifier.reset(scaleUpSpeed, getSprite().getScaleX(), 1.5f, getSprite().getScaleY(), 1.5f);
		jumpModifier.reset();
		resources.JumpSound.play();
	}
	
	public void Die()
	{
		playerSprite.stopAnimation(6);
		resources.PlayerCaughtSound.play();
		mPhysicsHandler.Stop();
	}
	
	public void Start()
	{
		long time = 100;
		long []timings = new long [] {time, time, time, time, time, time};
		playerSprite.animate(timings, 0, 5, true);
		setTargetAngle(90);
		mPhysicsHandler.Continue();
	}
	
	public void setTargetAngle(float pTargetAngle)
	{
		mPhysicsHandler.setTargetAngle(pTargetAngle);
	}
	
	private void loadMainSprite()
	{
		final float centerX = resources.camera.getWidth() / 2;
		final float centerY = resources.camera.getHeight() / 2;
		playerSprite = new PlayerAnimatedSprite(centerX, centerY, 67, 116, resources.PlayerTextureRegion, resources.vbom);
	}
	
	public class PlayerAnimatedSprite extends AnimatedSprite
	{	
		private IEntity spriteBody;
		
		public PlayerAnimatedSprite(float pX, float pY, float pWidth,
				float pHeight, ITiledTextureRegion pTiledTextureRegion,
				VertexBufferObjectManager pVertexBufferObjectManager) 
		{
			super(pX, pY, pWidth, pHeight, pTiledTextureRegion, pVertexBufferObjectManager);
			
			float bodyWidth = 5;
			float bodyHeight = 42;
			spriteBody = new Rectangle(pWidth/2-6, 40+bodyHeight/2+17, bodyWidth, bodyHeight, pVertexBufferObjectManager);
			spriteBody.setColor(Color.BLUE);
			spriteBody.setVisible(false);
			this.attachChild(spriteBody);
		}
		
		@Override
		public float getRotation() {
			return super.getRotation() - 90;
		}
		
		@Override
		public void setRotation(float pRotation) {
			super.setRotation(pRotation + 90);
		}
		
		public float getPolarRotation()
		{
			return DegreeConverter.EngineToPolar(this.getRotation());
		}
		
		public void setPolarRotation(float pAngle)
		{
			this.setRotation(DegreeConverter.PolarToEngine(pAngle));
		}
		
		public IEntity getBody()
		{
			return spriteBody;
		}
	}
	
	public enum JumpProgress
	{
		NoJump, GoUp, OnTop, GoDown
	}
}
