package com.test2d.entity;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.AnimatedSprite.IAnimationListener;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.util.adt.color.Color;

import com.test2d.ResourcesManager;
import com.test2d.engine.PolicePhysicsHandler;
import com.test2d.engine.TimerHandlerExtended;

import android.graphics.PointF;

public class Police extends Sprite{
	
	private ResourcesManager resources;
	private PolicePhysicsHandler policePhysics;
	private TimerHandlerExtended startChaseHandler;
	private AnimatedSprite splashBom;
	private AnimationListener animationListener;
	private IEntity spriteBody;
	private boolean isStopped = false;
	
	public Police(float pX, float pY, ResourcesManager pResources) 
	{
		super(pX, pY, pResources.PoliceTextureRegion, pResources.vbom);
		resources = pResources;
		
		animationListener = new AnimationListener(this);
		splashBom = createSplash();
		splashBom.setVisible(false);
		attachChild(splashBom);
		
		startChaseHandler = new TimerHandlerExtended(2f, new ITimerCallback() 
	    {
            public void onTimePassed(final TimerHandler pTimerHandler) 
            {
            	startChaseProcess();
            }
	    });
		resources.engine.registerUpdateHandler(startChaseHandler);
		
		float height = this.getHeight() - 8;
		float width = this.getWidth() - 8;
		
		spriteBody = new Rectangle(width/2 + 3, height/2 + 4, width, height, pResources.vbom);
		spriteBody.setColor(Color.RED);
		spriteBody.setVisible(false);
		this.attachChild(spriteBody);
	}
	
	@Override
	protected void onManagedUpdate(float pSecondsElapsed) 
	{
		if (!RandomPoint.isInsideBox(getX(), getY()) && isDriving())
			startChaseProcess();
		
		super.onManagedUpdate(pSecondsElapsed);
	}
	
	public void setPhycicsHandler(PolicePhysicsHandler handler)
	{
		policePhysics = handler;
		this.registerUpdateHandler(policePhysics);
	}
	
	public IEntity getBody()
	{
		return spriteBody;
	}
	
	public void bang()
	{
		policePhysics.Stop();
		splashBom.setVisible(true);
		splashBom.animate(new long[] {125, 125, 125, 125}, false, animationListener);
	}
	
	public void startChase()
	{
		isStopped = false;
		if (!startChaseHandler.isInProgress())
			startChaseHandler.reset();
	}
	
	public boolean isDriving()
	{
		return !policePhysics.isStopped();
	}
	
	public void Stop()
	{
		isStopped = true;
		policePhysics.Stop();
	}
	
	public IEntity getSplash()
	{
		return splashBom;
	}
	
	private void startChaseProcess()
	{
		if (isStopped)
			return;
		policePhysics.Continue();
		updatePosition();
		setVisible(true);
		splashBom.setVisible(false);
	}
	
	private void updatePosition()
	{		
		PointF newPos = RandomPoint.getOutsidePoint();
		this.setPosition(newPos.x, newPos.y);
	}
	
	private AnimatedSprite createSplash()
	{
		float scale = 1.5f;
		AnimatedSprite splash = new AnimatedSprite(50, 10, 102, 103, resources.SplashBomTextureRegion, resources.vbom);
		splash.setScale(scale);
		Text splashScore = new Text(splash.getScaleCenterX() - 10 + splash.getWidth() / 2, splash.getScaleCenterY() - 3 + splash.getHeight() / 2, resources.MainFont, "50", resources.vbom);
		splashScore.setScale(0.5f);
		splash.attachChild(splashScore);
		
		return splash;
	}
	
	@Override
	public void onDetached() {
		super.onDetached();
		resources.engine.unregisterUpdateHandler(startChaseHandler);
	}
	
	static class AnimationListener implements IAnimationListener
	{
		private IEntity entityToControl;
		
		public AnimationListener(IEntity pEntityToControl)
		{
			entityToControl = pEntityToControl;
		}
		
		@Override
		public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {}
		@Override
		public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {}
		@Override
		public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) 
		{ }

		@Override
		public void onAnimationFinished(AnimatedSprite pAnimatedSprite) 
		{
			entityToControl.setVisible(false);
		}
	}
}
