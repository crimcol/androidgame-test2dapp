package com.test2d.entity;

import java.util.Random;

import org.andengine.engine.camera.Camera;
import android.graphics.PointF;

public class RandomPoint {
	
	private static Camera m_Camera;
	private static Box m_Box;
	
	static
	{
		m_Box = new BoxWithBound(0, 0);
	}
	
	public static void setCamera(Camera camera)
	{
		m_Camera = camera;
		m_Box.setSize(m_Camera.getWidth(), m_Camera.getHeight());
	}
	
	public static void updateBoxPosition()
	{
		m_Box.setPosition(m_Camera.getXMin(), m_Camera.getYMin());
	}
	
	public static PointF getOutsidePoint()
	{
		float box_minX = m_Box.Pos.x;
		float box_maxX = m_Box.Pos.x + m_Box.Width;
		float box_minY = m_Box.Pos.y;
		float box_maxY = m_Box.Pos.y + m_Box.Height;
		
		float randomWidth = 100;
		float newX = 0;
		float newY = 0;
		
		Side side = Side.getRandom();
		if (side == Side.Right)
		{
			newX = box_maxX - (float)Math.random() * randomWidth;
			newY = box_minY + (float)Math.random() * (box_maxY - box_minY);
		}
		else if (side == Side.Left)
		{
			newX = box_minX + (float)Math.random() * randomWidth;
			newY = box_minY + (float)Math.random() * (box_maxY - box_minY);
		}
		else if (side == Side.Top)
		{
			newX = box_minX + (float)Math.random() * (box_maxX - box_minX);
			newY = box_maxY - (float)Math.random() * randomWidth;
		}
		else if (side == Side.Bottom)
		{
			newX = box_minX + (float)Math.random() * (box_maxX - box_minX);
			newY = box_minY + (float)Math.random() * randomWidth;
		}
		
		return new PointF(newX, newY);
	}
	
	public static boolean isInsideBox(float pX, float pY)
	{
		return m_Box.isInsideBox(pX, pY);
	}
	
	enum Side
	{
		Left, Right, Top, Bottom;
		
		private static Side [] values = new Side[] {Left, Right, Top, Bottom};
		private static Random random = new Random();
		
		public static Side getRandom()
		{
			return values[random.nextInt(values.length)];
		}
	}
}

class Box
{
	public PointF Pos;
	public float Width;
	public float Height;
	
	public Box(float width, float height)
	{
		setSize(width, height);
		setPosition(0, 0);
	}
	
	public boolean isInsideBox(float pX, float pY)
	{
		if (pX >= Pos.x && pX <= Pos.x + Width
				&& pY >= Pos.y && pY <= Pos.y + Height)
			return true;
		
		return false;
	}
	
	public void setSize(float width, float height)
	{
		Width = width;
		Height = height;
	}
	
	public void setPosition(float pX, float pY)
	{
		Pos = new PointF(pX, pY);
	}
}

class BoxWithBound extends Box
{
	private final static float BOUND_SIZE = 150;

	public BoxWithBound(float width, float height) {
		super(width, height);
	}
	
	@Override
	public void setPosition(float pX, float pY) {
		super.setPosition(pX - BOUND_SIZE, pY - BOUND_SIZE);
	}
	
	@Override
	public void setSize(float width, float height) {
		super.setSize(width + 2 * BOUND_SIZE, height + 2 * BOUND_SIZE);
	}
}
