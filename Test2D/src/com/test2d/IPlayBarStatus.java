package com.test2d;

public interface IPlayBarStatus {

	void reduceLive();
	void addScore(int pScore);
	void resetStatus();
}
