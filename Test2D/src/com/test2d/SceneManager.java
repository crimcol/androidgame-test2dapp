package com.test2d;

import org.andengine.engine.Engine;
import org.andengine.ui.IGameInterface.OnCreateSceneCallback;

import com.test2d.controllers.*;

import Scenes.BaseScene;
import Scenes.GameOverScene;
import Scenes.GamePlayScene;
import Scenes.StartScene;
import Scenes.TestScene;

public class SceneManager {
	
	//---------------------------------------------
    // SCENES
    //---------------------------------------------
    
    private BaseScene m_testScene;
    private BaseScene m_startScene;
    private BaseScene m_gamePlayScene;
    private BaseScene m_gameOverScene;
	private BaseScene splashScene;
    private BaseScene menuScene;
    private BaseScene gameScene;
    private BaseScene loadingScene;
    
    //---------------------------------------------
    // VARIABLES
    //---------------------------------------------
    
    private static final SceneManager INSTANCE = new SceneManager();
    
    private SceneType currentSceneType = SceneType.SCENE_SPLASH;
    private BaseScene currentScene;
    
    private ResourcesManager resourseManager;
    private Engine engine;
    
    public enum SceneType
    {
        SCENE_TEST,
        SCENE_START,
        SCENE_GAMEPLAY,
        SCENE_GAMEOVER,
    	SCENE_SPLASH,
        SCENE_MENU,
        SCENE_GAME,
        SCENE_LOADING,
    }
    
    //---------------------------------------------
    // CLASS LOGIC
    //---------------------------------------------
    
    public SceneManager()
    {
    	resourseManager = ResourcesManager.getInstance();
    	engine = resourseManager.engine;
    }
    
    private void setScene(BaseScene scene)
    {
        engine.setScene(scene);
        currentScene = scene;
        currentSceneType = scene.getSceneType();
    }
    
    public void setScene(SceneType sceneType)
    {
        switch (sceneType)
        {
        	case SCENE_TEST:
        		setScene(m_testScene);
        		break;
        	case SCENE_START:
        		setScene(m_startScene);
        		break;
        	case SCENE_GAMEPLAY:
        		setScene(m_gamePlayScene);
        		break;
        	case SCENE_GAMEOVER:
        		break;
        	case SCENE_MENU:
                setScene(menuScene);
                break;
            case SCENE_GAME:
                setScene(gameScene);
                break;
            case SCENE_SPLASH:
                setScene(splashScene);
                break;
            case SCENE_LOADING:
                setScene(loadingScene);
                break;
            default:
                break;
        }
    }
    
    //---------------------------------------------
    // GETTERS AND SETTERS
    //---------------------------------------------
    
    public static SceneManager getInstance()
    {
        return INSTANCE;
    }
    
    public SceneType getCurrentSceneType()
    {
        return currentSceneType;
    }
    
    public BaseScene getCurrentScene()
    {
        return currentScene;
    }
    
    public void createTestScene(OnCreateSceneCallback pOnCreateSceneCallback)
    {
    	resourseManager.loadTestScreen();
        m_testScene = new TestScene(resourseManager);
        pOnCreateSceneCallback.onCreateSceneFinished(m_testScene);
    }
    
    public void createStartScene(StartController pController)
    {
    	resourseManager.loadStartScene();
    	m_startScene = new StartScene(pController, resourseManager);
    	setScene(m_startScene);
    }
    
    public void disposeStartScene()
    {
    	m_startScene.disposeScene();
    	m_startScene = null;
    	resourseManager.unloadStartScene();
    }
    
    public void createGamePlayScene(GamePlayController pController)
    {
    	resourseManager.loadGamePlayScene();
    	m_gamePlayScene = new GamePlayScene(pController, resourseManager);
    	setScene(m_gamePlayScene);    	
    }
    
    public void disposeGamePlayScene()
    {
    	m_gamePlayScene.disposeScene();
    	m_gamePlayScene = null;
    	resourseManager.unloadGamePlayScene();
    }
    
    public void createGameOverScene(GameOverController pController)
    {
    	resourseManager.loadGameOverScene();
    	m_gameOverScene = new GameOverScene(pController, resourseManager);
    	setScene(m_gameOverScene);
    }
    
    public void disposeGameOverScene()
    {
    	m_gameOverScene.disposeScene();
    	m_gameOverScene = null;
    	resourseManager.unloadGameOverScene();
    }
    /*
    private void disposeTestScene()
    {
        ResourcesManager.getInstance().unloadTestScreen();
        m_testScene.disposeScene();
        m_testScene = null;
    }*/
}
