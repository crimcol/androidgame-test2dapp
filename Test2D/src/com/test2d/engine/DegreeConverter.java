package com.test2d.engine;

public class DegreeConverter {

	public static float EngineToPolar(float pEngineAngle)
	{
		float angle = pEngineAngle;
		while(angle >= 360)
			angle -= 360;
		while (angle <= -360)
			angle += 360;
		
		return 360 - angle;
	}
	
	public static float PolarToEngine(float pPolarAngle)
	{
		return EngineToPolar(pPolarAngle);
	}
	
	public static float PolarByPoint(float pX, float pY)
	{
		double angleRad = 0;
		float x = pX;
		float y = pY;
		
		if (x > 0 && y >= 0)
			angleRad = Math.atan(y/x);
		else if (x > 0 && y < 0)
			angleRad = Math.atan(y/x) + 2 * Math.PI;
		else if (x < 0)
			angleRad = Math.atan(y/x) + Math.PI;
		else if (x == 0 && y > 0)
			angleRad = Math.PI / 2;
		else if (x == 0 && y < 0)
			angleRad = Math.PI * 3 / 2;
		else if (x == 0 && y == 0)
			angleRad = 0;
		
		return 360 - (float)Math.toDegrees(angleRad);
	}
}
