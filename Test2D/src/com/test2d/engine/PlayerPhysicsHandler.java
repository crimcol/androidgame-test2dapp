package com.test2d.engine;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.IEntity;


public class PlayerPhysicsHandler extends PhysicsHandler
{
	private float m_RotationSpeed = 150f;
	private float m_Speed;
	private float m_TargretAngle;
	
	protected boolean isStopped;
	
	public PlayerPhysicsHandler(IEntity pEntity) {
		super(pEntity);
		
		setTargetAngle(DegreeConverter.EngineToPolar(pEntity.getRotation()));
		setSpeed(200);
	}
	
	public void setSpeed(float pSpeed)
	{
		m_Speed = pSpeed;
	}
	
	public float getSpeed()
	{
		return m_Speed;
	}
	
	public void setRotationSpeed(float pRotationSpeed)
	{
		m_RotationSpeed = pRotationSpeed;
	}
	
	public float getRotationSpeed()
	{
		return m_RotationSpeed;
	}
	
	public void setTargetAngle(float pTargetAngle)
	{
		m_TargretAngle = pTargetAngle;
	}
	
	public float getTargetAngle()
	{
		return m_TargretAngle;
	}
	
	public void Stop()
	{
		isStopped = true;
	}
	
	public boolean isStopped()
	{
		return isStopped;
	}
	
	public void Continue()
	{
		isStopped = false;
	}
	
	@Override
	public float getVelocityX() {
		if (isStopped())
			return 0;
		return super.getVelocityX();
	}
	
	@Override
	public float getVelocityY() {
		if (isStopped())
			return 0;
		return super.getVelocityY();
	}
	
	@Override
	protected void onUpdate(float pSecondsElapsed, IEntity pEntity)
	{
		if (isStopped)
			return;
		
		float currentRotation = getFixedRotation(pEntity);
		
		float minAngle = getMinAngle(currentRotation, this.getTargetAngle());
		float angleSign = Math.signum(minAngle);
		
		/* Apply target angle to angular velocity. */
		
		if (Math.abs(this.getTargetAngle() - currentRotation) > 2f)
			this.setAngularVelocity(m_RotationSpeed * angleSign);
		else
			this.setAngularVelocity(0f);		
		
		/* Apply angular velocity. */
		final float angularVelocity = this.mAngularVelocity;
		if (angularVelocity != 0) {
			pEntity.setRotation(
					DegreeConverter.PolarToEngine(
							DegreeConverter.EngineToPolar(pEntity.getRotation()) + angularVelocity * pSecondsElapsed
							));
		}
		
		/* Apply rotation to velocity. */
		float rotation = getFixedRotation(pEntity);
		this.mVelocityX = (float)Math.cos(Math.toRadians(rotation)) * getSpeed();
		this.mVelocityY = (float)Math.sin(Math.toRadians(rotation)) * getSpeed();
		
		/* Apply linear acceleration. */
		final float accelerationX = this.mAccelerationX;
		final float accelerationY = this.mAccelerationY;
		if (accelerationX != 0 || accelerationY != 0) {
			this.mVelocityX += accelerationX * pSecondsElapsed;
			this.mVelocityY += accelerationY * pSecondsElapsed;
		}		

		/* Apply linear velocity. */
		final float velocityX = this.mVelocityX;
		final float velocityY = this.mVelocityY;
		if (velocityX != 0 || velocityY != 0) {
			pEntity.setPosition(pEntity.getX() + velocityX * pSecondsElapsed, pEntity.getY() + velocityY * pSecondsElapsed);
		}
	}
	
	private float getFixedRotation(IEntity pEntity)
	{
		float currentRotation = DegreeConverter.EngineToPolar(pEntity.getRotation());
		return currentRotation;
	}
	
	private float getMinAngle(float sourseAngle, float targetAngle)
	{
		float diff = targetAngle - sourseAngle;
		float sign = Math.signum(diff);
		
		float minAngle = diff;
		if (Math.abs(diff) > 180)
		{
			minAngle = 360 - Math.abs(diff); 
			minAngle *= (sign * (-1));
		}		
		return minAngle;
	}
}
