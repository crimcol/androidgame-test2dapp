package com.test2d.engine;

import android.annotation.SuppressLint;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.scene.background.Background;
import org.andengine.opengl.util.GLState;

@SuppressLint("WrongCall") 
public class BackgroundCustom extends Background {

	private IEntity m_Background;
	private float m_BackgroundWidth;
	private float m_BackgroundHeigth;
	private float m_ShiftX;
	private float m_ShiftY;
	private float m_LocalShiftX;
	private float m_LocalShiftY;
	
	private PhysicsHandler m_UpdateHandler;
	
	public BackgroundCustom(IEntity pEntity, float pBackgroundWidth, float pBackgroundHeigth)
	{
		super();
		m_BackgroundWidth = pBackgroundWidth;
		m_BackgroundHeigth = pBackgroundHeigth;
		m_Background = pEntity;
	}
	
	public void setShiftXY(float pShiftX, float pShiftY)
	{
		m_ShiftX = pShiftX;
		m_ShiftY = pShiftY;
		updateLocalShift();
	}
	
	public void setUpdateHandler(PhysicsHandler pHandler)
	{
		m_UpdateHandler = pHandler;
	}
	
	@Override
	public void onUpdate(float pSecondsElapsed) {
		super.onUpdate(pSecondsElapsed);
		
		if (m_UpdateHandler == null)
			return;
		
		m_LocalShiftX += -1 * m_UpdateHandler.getVelocityX() * pSecondsElapsed;
		m_LocalShiftY += -1 * m_UpdateHandler.getVelocityY() * pSecondsElapsed;
	}	
	
	@Override
	public void onDraw(GLState pGLState, Camera pCamera) {
		super.onDraw(pGLState, pCamera);
		
		onDraw(pGLState, pCamera, this.m_LocalShiftX, this.m_LocalShiftY);
	}
	
	public void onDraw(final GLState pGLState, final Camera pCamera, final float pShiftX, final float pShiftY) {		
		pGLState.pushModelViewGLMatrix();
		{
			final float backgroundWidth = m_BackgroundWidth;
			final float entityWidthScaled = this.m_Background.getWidth() * this.m_Background.getScaleX();
			float baseOffsetX = pShiftX % entityWidthScaled;

			while (baseOffsetX > 0) {
				baseOffsetX -= entityWidthScaled;
			}
			//========================================
			final float backgroundHeight = m_BackgroundHeigth;
			final float entityHeightScaled = this.m_Background.getHeight() * this.m_Background.getScaleY();
			float baseOffsetY = pShiftY % entityHeightScaled;

			while (baseOffsetY > 0) {
				baseOffsetY -= entityHeightScaled;
			}
			
			pGLState.translateModelViewGLMatrixf(baseOffsetX, baseOffsetY, 0);			
			float currentMaxY = baseOffsetY;
			
			do{
				float currentMaxX = baseOffsetX;
				
				do {
					this.m_Background.onDraw(pGLState, pCamera);
					pGLState.translateModelViewGLMatrixf(entityWidthScaled, 0, 0);
					currentMaxX += entityWidthScaled;
				} while (currentMaxX < backgroundWidth);
				
				pGLState.translateModelViewGLMatrixf(-(currentMaxX - baseOffsetX), 0, 0);
				pGLState.translateModelViewGLMatrixf(0, entityHeightScaled, 0);
				currentMaxY += entityHeightScaled;
			} while (currentMaxY < backgroundHeight);
		}
		pGLState.popModelViewGLMatrix();
	}
	
	private void updateLocalShift()
	{
		m_LocalShiftX += m_ShiftX;
		m_LocalShiftY += m_ShiftY;
	}
}
