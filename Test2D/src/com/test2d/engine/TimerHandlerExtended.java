package com.test2d.engine;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;

public class TimerHandlerExtended extends TimerHandler {
	
	private boolean m_IsEnabled;
	
	public TimerHandlerExtended(float pTimerSeconds,
			ITimerCallback pTimerCallback) {
		this(pTimerSeconds, new TimerCallbackWrapper(pTimerCallback), true);
	}
	
	public TimerHandlerExtended(float pTimerSeconds,
			ITimerCallback pTimerCallback, boolean isEnabled) {
		super(pTimerSeconds, new TimerCallbackWrapper(pTimerCallback));
		m_IsEnabled = isEnabled;
	}

	@Override
	public void onUpdate(float pSecondsElapsed) {
		if (m_IsEnabled)
			super.onUpdate(pSecondsElapsed);
	}
	
	public boolean isInProgress()
	{
		return ((TimerCallbackWrapper)this.mTimerCallback).isInProgress();
	}
	
	public void Enable()
	{
		m_IsEnabled = true;
	}
	
	public void Disable()
	{
		m_IsEnabled = false;
	}
	
	@Override
	public void reset() {
		Enable();
		super.reset();
	}
	
	static class TimerCallbackWrapper implements ITimerCallback
	{
		private boolean isInProgress;
		private ITimerCallback externalCallback;
		
		public TimerCallbackWrapper(ITimerCallback pExternalCallback)
		{
			externalCallback = pExternalCallback;
		}
		
		@Override
		public void onTimePassed(TimerHandler pTimerHandler) {
			isInProgress = true;
			externalCallback.onTimePassed(pTimerHandler);
			isInProgress = false;
		}
		
		public boolean isInProgress()
		{
			return isInProgress;
		}
	}
}
