package com.test2d.engine;

import org.andengine.entity.IEntity;

import com.test2d.entity.Player;

public class PolicePhysicsHandler extends PlayerPhysicsHandler
{
	private Player m_Player;
	
	public PolicePhysicsHandler(IEntity pEntity, Player pPlayer) 
	{
		super(pEntity);
		setRotationSpeed(80);
		setSpeed(240);
		m_Player = pPlayer;
	}
	
	@Override
	protected void onUpdate(float pSecondsElapsed, IEntity pEntity) 
	{
		float sX = getEntity().getX();
		float sY = getEntity().getY();
		float tX = m_Player.getSprite().getX();
		float tY = m_Player.getSprite().getY();
		
		float targetAngle = DegreeConverter.PolarByPoint(tX - sX, sY - tY);		
		setTargetAngle(targetAngle);
		
		super.onUpdate(pSecondsElapsed, pEntity);
	}
}
