package Scenes;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.animator.AlphaMenuSceneAnimator;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.TextMenuItem;
import org.andengine.entity.scene.menu.item.decorator.*;
import org.andengine.entity.text.Text;
import org.andengine.util.adt.color.Color;

import com.test2d.ResourcesManager;
import com.test2d.SceneManager.SceneType;
import com.test2d.controllers.StartController;

public class StartScene extends BaseScene implements IOnMenuItemClickListener {
	
	private static final int MENU_PLAY = 0;
	private static final int MENU_QUIT = MENU_PLAY + 1;
	
	private MenuScene mMenuScene;
	private StartController m_controller;

	public StartScene(StartController pController, ResourcesManager pRecources)
	{
		super(pRecources);
		m_controller = pController;
	}
	
	@Override
	public void createScene() {
		float cameraCenterX = resources.camera.getWidth()/2;
		float cameraCenterY = resources.camera.getHeight()/2;
		
		setBackground(new Background(Color.BLACK));
	    attachChild(new Text(cameraCenterX, cameraCenterY, resources.MainFont, "EL EMIGRANTE", resources.vbom));
	 
	    mMenuScene = this.createMenuScene();
	    mMenuScene.setY(resources.camera.getHeight() / -4);
	    setChildScene(mMenuScene, false, true, true);
	}

	@Override
	public void onBackKeyPressed() {
		m_controller.closeGame();
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_START;
	}

	@Override
	public void disposeScene() {
		this.detachChild(mMenuScene);
	}
	
	protected MenuScene createMenuScene() {
		final MenuScene menuScene = new MenuScene(resources.camera, new AlphaMenuSceneAnimator());

		final IMenuItem playMenuItem = new ScaleMenuItemDecorator(new TextMenuItem(MENU_PLAY, resources.MainFont, "play ", resources.vbom), 1.2f, 1.0f);
		menuScene.addMenuItem(playMenuItem);

		final IMenuItem quitMenuItem = new ColorMenuItemDecorator(new TextMenuItem(MENU_QUIT, resources.MainFont, "quit", resources.vbom), Color.RED, Color.WHITE);
		menuScene.addMenuItem(quitMenuItem);

		menuScene.buildAnimations();

		menuScene.setBackgroundEnabled(false);

		menuScene.setOnMenuItemClickListener(this);
		return menuScene;
	}

	@Override
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem,
			float pMenuItemLocalX, float pMenuItemLocalY) {
		
		switch(pMenuItem.getID()) {
			case MENU_PLAY:
				m_controller.goToGamePlay();
				return true;
			case MENU_QUIT:
				m_controller.closeGame();
				return true;
			default:
				return false;
		}
	}
}
