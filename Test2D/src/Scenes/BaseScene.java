package Scenes;

import org.andengine.entity.scene.Scene;

import com.test2d.ResourcesManager;
import com.test2d.SceneManager.SceneType;

public abstract class BaseScene extends Scene {

	//---------------------------------------------
    // VARIABLES
    //---------------------------------------------
    
    protected ResourcesManager resources;
    
    //---------------------------------------------
    // CONSTRUCTOR
    //---------------------------------------------
    
    public BaseScene(ResourcesManager pResources)
    {
        resources = pResources;
        createScene();
    }
    
    //---------------------------------------------
    // ABSTRACTION
    //---------------------------------------------
    
    public abstract void createScene();
    
    public abstract void onBackKeyPressed();
    
    public abstract SceneType getSceneType();
    
    public abstract void disposeScene();
}
