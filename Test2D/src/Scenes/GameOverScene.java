package Scenes;

import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.text.Text;
import org.andengine.util.adt.color.Color;

import com.test2d.ResourcesManager;
import com.test2d.SceneManager.SceneType;
import com.test2d.controllers.GameOverController;

public class GameOverScene extends BaseScene 
{
	private GameOverController m_controller;
	
	public GameOverScene(GameOverController pController, ResourcesManager pResources)
	{
		super(pResources);
		m_controller = pController;
	}
	
	@Override
	public void createScene() 
	{	
		float cameraCenterX = resources.camera.getWidth()/2;
		float cameraCenterY = resources.camera.getHeight()/2;
		
		Text text = new Text(cameraCenterX, cameraCenterY, resources.MainFont, "Game Over", resources.vbom);
		text.setColor(Color.WHITE);
		attachChild(text);
		
		ButtonSprite newGameButton = new ButtonSprite(
				resources.NewGameButton.getWidth() / 2, 
				resources.NewGameButton.getHeight() / 2, 
				resources.NewGameButton,
				resources.vbom,
				new OnClickListener() {
					@Override
					public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX, float pTouchAreaLocalY) {
						m_controller.goToGamePlay();
					}
				});
		attachChild(newGameButton);
		this.registerTouchArea(newGameButton);
		
		Text newGameText = new Text(newGameButton.getWidth() / 2, newGameButton.getHeight() / 2 - 5, resources.ButtonFont, "New Game?", resources.vbom);
		newGameText.setColor(Color.BLACK);
		newGameButton.attachChild(newGameText);
	}

	@Override
	public void onBackKeyPressed() {
		m_controller.goToStartScene();
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_GAMEOVER;
	}

	@Override
	public void disposeScene() {
	}

}
