package Scenes;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.AnimatedSprite;

import com.test2d.ResourcesManager;
import com.test2d.SceneManager.SceneType;

public class TestScene extends BaseScene {
	
	public TestScene(ResourcesManager pResources)
	{
		super(pResources);
	}

	private AnimatedSprite m_sprite;

	@Override
	public void createScene() {
		setBackground(new Background(0.09804f, 0.6274f, 0.8784f));
		m_sprite = new AnimatedSprite(100, 100, resources.BrontTextureRegion, resources.vbom);		
		attachChild(m_sprite);
	}

	@Override
	public void onBackKeyPressed() {
		disposeScene();
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_TEST;
	}

	@Override
	public void disposeScene() {
		m_sprite.detachSelf();
	    m_sprite.dispose();
	    this.detachSelf();
	    this.dispose();
	}

}
