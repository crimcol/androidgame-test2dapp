package Scenes;

import java.util.ArrayList;
import java.util.List;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.util.adt.color.Color;

import com.test2d.IPlayScene;
import com.test2d.PlayBarStatus;
import com.test2d.ResourcesManager;
import com.test2d.SceneManager.SceneType;
import com.test2d.controllers.GamePlayController;
import com.test2d.engine.BackgroundCustom;
import com.test2d.engine.PolicePhysicsHandler;
import com.test2d.entity.Player;
import com.test2d.entity.Police;
import com.test2d.entity.Stone;

public class GamePlayScene extends BaseScene implements IPlayScene 
{	
	private PlayBarStatus barStatus;
	private GamePlayController gameLogic;
	private Text debugTestCurrentAngle;
	
	public GamePlayScene(GamePlayController pController, ResourcesManager pResources)
	{
		super(pResources);
		gameLogic = pController;
	}
	
	public void LazyInit()
	{
		createBackgroundCustom().setUpdateHandler(gameLogic.player.mPhysicsHandler);
		resources.camera.setChaseEntity(gameLogic.player.getSprite());
		this.setOnSceneTouchListener(gameLogic);
		/* The actual collision-checking. */
		this.registerUpdateHandler(gameLogic);
	}
	
	public Player createPlayer()
	{
		Player p = new Player(resources);
		p.load();
		
		this.getPlayerLayer().attachChild(p.getSprite());
		//p.Start();
		
		return p;
	}
	
	public List<Stone> createStones()
	{
		List<Stone> stoneList = new ArrayList<Stone>();
		for (int i=0; i<10; i++)
		{
			Stone s = new Stone(0, 0, resources);
			stoneList.add(s);
			this.getStonesLayer().attachChild(s);
		}
		return stoneList;
	}
	
	public List<Police> createPolice()
	{
		List<Police> cars = new ArrayList<Police>();
		
		for (int i = 0; i<5; i++)
		{
			Police car = new Police((float) (Math.random()*600), (float) (Math.random()*400), resources);
			PolicePhysicsHandler physics = new PolicePhysicsHandler(car, gameLogic.player);
			car.setPhycicsHandler(physics);
			cars.add(car);
			this.getPoliceLayer().attachChild(car);
		}
		
		return cars;
	}
	
	@Override
	public void createScene() 
	{	
		attachChild(new Entity());	// stones
		attachChild(new Entity());	// cars
		attachChild(new Entity());	// player
		
		barStatus = new PlayBarStatus(resources);
		resources.camera.setHUD(barStatus);
		
		debugTestCurrentAngle = new Text(300, 30, resources.MainFont, "0.0", 20, resources.vbom);
		debugTestCurrentAngle.setColor(Color.RED);
		barStatus.attachChild(debugTestCurrentAngle);
	}

	@Override
	public void onBackKeyPressed() {
		gameLogic.goToStartScene();
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_GAMEPLAY;
	}

	@Override
	public void disposeScene() {
		resources.camera.setCenter(resources.camera.getWidth()/2, resources.camera.getHeight()/2);		
		resources.camera.setChaseEntity(null);
		resources.camera.setHUD(null);
	}
	
	private BackgroundCustom createBackgroundCustom()
	{
		Sprite backgroundSprite = getSpriteForBackground();
		
		BackgroundCustom background = new BackgroundCustom(backgroundSprite, 
				resources.camera.getWidth() + backgroundSprite.getWidth() * 1, 
				resources.camera.getHeight() + backgroundSprite.getHeight() * 1);
		this.setBackground(background);
		
		return background;
	}
	
	private Sprite getSpriteForBackground()
	{
		ITextureRegion backgroundTextureRegion = resources.BackgroundTextureRegion;
		float width = backgroundTextureRegion.getWidth() * 1;
		float height = backgroundTextureRegion.getHeight() * 1;
		
		backgroundTextureRegion.setTextureSize(width, height);
		Sprite backgroundSprite = new Sprite(
				0, 0, 
				width, height, 
				backgroundTextureRegion, resources.vbom);
		
		return backgroundSprite;
	}
	
	@Override
	public IEntity getStonesLayer()
	{
		return getChildByIndex(0);
	}
	
	@Override
	public IEntity getPoliceLayer()
	{
		return getChildByIndex(1);
	}
	
	@Override
	public IEntity getPlayerLayer()
	{
		return getChildByIndex(2);
	}

	@Override
	public void setDebugText(String text) {
		debugTestCurrentAngle.setText(text);
	}

	@Override
	public void setLifes(int count) {
		if (count > -1)
		{
			barStatus.reduceLive();
		}
		else
		{
			gameLogic.goToGameOverScene();
		}
	}

	@Override
	public void addScore(int pScore) {
		barStatus.addScore(pScore);
	}
}
